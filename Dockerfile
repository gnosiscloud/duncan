FROM node:alpine

ENV SSHKEY "duncan"
ENV SSHCOMMENT "$SSHKEY ssh key for gitlab-ci"

RUN apk add --no-cache alpine-sdk build-base gzip bzip2 tar libxml2 \
libxml2-dev libxslt-dev libzip-dev nodejs npm openssh-client openssh

RUN npm update

RUN mkdir ~/.ssh

RUN echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

RUN ssh-keygen -t rsa -b 4096 -C "$SSHCOMMENT" -N "" -f ~/.ssh/"$SSHKEY"

RUN ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

RUN chmod 644 ~/.ssh/known_hosts && \
    chmod 700 ~/.ssh

RUN cat ~/.ssh/"$SSHKEY".pub

RUN npm install -g @angular/cli@latest