# Duncan

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

## Accessibility

[WAI-ARIA Authoring Practices 1.2](https://w3c.github.io/aria-practices/)

[Web Content Accessibility Guidelines](https://www.w3.org/TR/WCAG21/)

[Web Accessibility In Mind](https://webaim.org/)

[WebAIM's WCAG 2 Checklist](https://webaim.org/standards/wcag/checklist#sc1.4.4)